const { Router } = require('express')
const UserService = require('../services/userService')
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware')
const { responseMiddleware } = require('../middlewares/response.middleware')

const router = Router()

// TODO: Implement route controllers for user
router.get('/',(req, res, next) => {
    try {
        const data = UserService.getAll()
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.get('/:id',(req, res, next) => {
    try {
        const ID  = req.params.id
        const data = UserService.search({id: ID})
        if(!data)
            throw Error("User not found")
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.post('/',createUserValid, (req, res, next) => {
    try {
        if(!res.err){
            const data = req.body
            res.data = UserService.create(data)
        }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.put('/:id',updateUserValid, (req, res, next) => {
    try {
        if(!res.err){
            const data = req.body
            const id  = req.params.id
            res.data = UserService.update(id,data)
        }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.delete('/:id',(req, res, next) => {
    try {
        const ID  = req.params.id
        const data = UserService.delete(ID)
        if(data)
            res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
module.exports = router