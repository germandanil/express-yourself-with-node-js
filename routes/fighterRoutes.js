const { Router } = require('express')
const FighterService = require('../services/fighterService')
const { responseMiddleware } = require('../middlewares/response.middleware')
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware')

const router = Router()

// TODO: Implement route controllers for fighter
router.get('/:id',(req, res, next) => {
    try {
        const ID  = req.params.id
        const data = FighterService.search({id: ID})
        if(!data)
            throw Error("Fighter not found")
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.get('/', (req, res, next) => {
    try {
        const data = FighterService.getAll()
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.post('/',createFighterValid, (req, res, next) => {
    try {
        if(!res.err){
            const data = req.body
            res.data = FighterService.create(data)
        }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.put('/:id',updateFighterValid, (req, res, next) => {
    try {
        if(!res.err){
            const data = req.body
            const id  = req.params.id

            res.data = FighterService.update(id,data)
        }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.delete('/:id',(req, res, next) => {
    try {
        const ID  = req.params.id
        const data = FighterService.delete(ID)
        if(data)
            res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
module.exports = router
