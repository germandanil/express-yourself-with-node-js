import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
export function showWinnerModal(fighter) {
    const Winner = createFighterImage(fighter);
    showModal({
        title: ((`${fighter.name} WINNER!`).toUpperCase()),
        bodyElement: Winner,
        onClose: () => { window.location.reload(); }
    });
}