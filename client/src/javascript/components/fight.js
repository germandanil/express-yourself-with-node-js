import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        let winner = null
        let indF = document.getElementById(`left-fighter-indicator`)
        let indS = document.getElementById(`right-fighter-indicator`)
        let firstInf = {
            satusBlock: false,
            satusAttack: false,
            buttComb1: false,
            buttComb2: false,
            buttComb3: false,
            defense: firstFighter.defense,
            power: firstFighter.power,
            HP: firstFighter.health,
            HPbegin: firstFighter.health,
            LastTimeCrit: Date.now() - 10001
        }
        let secondInf = {
            satusBlock: false,
            satusAttack: false,
            buttComb1: false,
            buttComb2: false,
            buttComb3: false,
            defense: secondFighter.defense,
            power: secondFighter.power,
            HP: secondFighter.health,
            HPbegin: secondFighter.health,
            LastTimeCrit: Date.now() - 10001
        }
        const keyUp = (event) => {
            switch (event.code) {
                case (controls.PlayerOneAttack):
                    {
                        DisableanimationAttack('left')
                        firstInf.satusAttack = false
                        break
                    }
                case (controls.PlayerOneBlock):
                    { DisableanimationBlock('left');firstInf.satusBlock = false;console.log("First already not block"); break }
                case (controls.PlayerTwoAttack):
                    {
                        DisableanimationAttack('right')
                        secondInf.satusAttack = false;
                        break
                    }
                case (controls.PlayerTwoBlock):
                    { DisableanimationBlock('right');secondInf.satusBlock = false;console.log("Second already not block"); break }
                case (controls.PlayerOneCriticalHitCombination[0]):
                    { DisableanimationSuperAttack('left');offCombo(firstInf); break }
                case (controls.PlayerOneCriticalHitCombination[1]):
                    { DisableanimationSuperAttack('left');offCombo(firstInf); break }
                case (controls.PlayerOneCriticalHitCombination[2]):
                    { DisableanimationSuperAttack('left');offCombo(firstInf); break }

                case (controls.PlayerTwoCriticalHitCombination[0]):
                    { DisableanimationSuperAttack('right');offCombo(secondInf); break }
                case (controls.PlayerTwoCriticalHitCombination[1]):
                    { DisableanimationSuperAttack('right');offCombo(secondInf); break }
                case (controls.PlayerTwoCriticalHitCombination[2]):
                    { DisableanimationSuperAttack('right');offCombo(secondInf); break }
                default:
                    { break }
            }
        }
        const keyDown = (event) => {
            switch (event.code) {
                case (controls.PlayerOneAttack):
                    {
                        if (firstInf.satusAttack == false && firstInf.satusBlock == false) {
                            let D = 0
                            animationAttack('left')
                            if (secondInf.satusBlock == false)
                                D = getDamage(firstFighter, secondFighter)
                            console.log("Damage = " + D)
                            secondInf.HP -= D;
                            reloadHPbar(secondInf, indS);
                            offCombo(firstInf);
                            firstInf.satusAttack = true
                        }
                        break
                    }
                case (controls.PlayerOneBlock):
                    { animationBlock('left');firstInf.satusBlock = true;console.log("First get block");offCombo(firstInf); break }
                case (controls.PlayerTwoAttack):
                    {
                        console.log("ATTACK TWO")
                        if (secondInf.satusAttack == false && secondInf.satusBlock == false) {
                            animationAttack('right')
                            let D = 0
                            if (firstInf.satusBlock == false)
                                D = getDamage(secondFighter, firstFighter)
                            console.log("Damage = " + D)
                            firstInf.HP -= D;
                            reloadHPbar(firstInf, indF);
                            offCombo(secondInf);
                            secondInf.satusAttack = true;
                        }
                        break
                    }
                case (controls.PlayerTwoBlock):
                    { animationBlock('right');secondInf.satusBlock = true;console.log("Second get block");offCombo(secondInf); break }
                case (controls.PlayerOneCriticalHitCombination[0]):
                    {
                        firstInf.buttComb1 = true;
                        if (checkCombo(firstInf)) {
                            if (checkCoolDown(firstInf)) {
                                animationSuperAttack('left')
                                let attack = firstFighter.power
                                attack = attack * 2
                                console.log("Damage = " + attack)
                                secondInf.HP -= attack;
                                reloadHPbar(secondInf, indS);
                                firstInf.LastTimeCrit = Date.now()
                                offCombo(firstInf);
                            }
                        }
                        break
                    }
                case (controls.PlayerOneCriticalHitCombination[1]):
                    {
                        firstInf.buttComb2 = true;
                        if (checkCombo(firstInf)) {
                            if (checkCoolDown(firstInf)) {
                                animationSuperAttack('left')
                                let attack = firstFighter.power
                                attack = attack * 2
                                console.log("Damage = " + attack)
                                secondInf.HP -= attack;
                                reloadHPbar(secondInf, indS);
                                firstInf.LastTimeCrit = Date.now()
                                offCombo(firstInf);
                            }
                        }
                        break
                    }
                case (controls.PlayerOneCriticalHitCombination[2]):
                    {
                        firstInf.buttComb3 = true;
                        if (checkCombo(firstInf)) {
                            if (checkCoolDown(firstInf)) {
                                animationSuperAttack('left')
                                console.log(checkCoolDown(secondInf))
                                let attack = firstFighter.power
                                attack = attack * 2
                                console.log("Damage = " + attack)
                                secondInf.HP -= attack;
                                reloadHPbar(secondInf, indS);
                                firstInf.LastTimeCrit = Date.now()
                                offCombo(firstInf);
                            }
                        }
                        break
                    }

                case (controls.PlayerTwoCriticalHitCombination[0]):
                    {
                        secondInf.buttComb1 = true;
                        if (checkCombo(secondInf)) {
                            if (checkCoolDown(secondInf)) {

                                animationSuperAttack('right')
                                let attack = secondInf.power
                                attack = attack * 2
                                console.log("Damage = " + attack)
                                firstInf.HP -= attack;
                                reloadHPbar(firstInf, indF);
                                secondInf.LastTimeCrit = Date.now()
                                offCombo(secondInf);
                            }
                        }
                        break
                    }
                case (controls.PlayerTwoCriticalHitCombination[1]):
                    {
                        secondInf.buttComb2 = true;
                        if (checkCombo(secondInf)) {
                            if (checkCoolDown(secondInf)) {
                                animationSuperAttack('right')
                                let attack = secondInf.power
                                attack = attack * 2
                                console.log("Damage = " + attack)
                                firstInf.HP -= attack;
                                reloadHPbar(firstInf, indF);
                                secondInf.LastTimeCrit = Date.now()
                                offCombo(secondInf);
                            }
                        }
                        break
                    }
                case (controls.PlayerTwoCriticalHitCombination[2]):
                    {

                        secondInf.buttComb3 = true;
                        if (checkCombo(secondInf)) {
                            if (checkCoolDown(secondInf)) {
                                animationSuperAttack('right')
                                let attack = secondInf.power
                                attack = attack * 2
                                console.log("Damage = " + attack)
                                firstInf.HP -= attack;
                                reloadHPbar(firstInf, indF);
                                secondInf.LastTimeCrit = Date.now()
                                offCombo(secondInf);
                            }
                        }
                        break
                    }
                default:
                    { offCombo(firstInf);offCombo(secondInf); break }
            }
            if (firstInf.HP <= 0) {
                document.removeEventListener('keydown', keyDown);
                document.removeEventListener('keyup', keyUp);
                console.log(secondFighter.name)
                resolve(secondFighter)
            }
            if (secondInf.HP <= 0) {
                console.log(firstFighter.name)
                document.removeEventListener('keydown', keyDown);
                document.removeEventListener('keyup', keyUp);
                resolve(firstFighter)
            }


        }
        document.addEventListener('keydown', keyDown)
        document.addEventListener('keyup', keyUp)
    });
}

export function getDamage(attacker, defender) {
    const damage = getHitPower(attacker) - getBlockPower(defender)
    return damage > 0 ? damage : 0
}

export function getHitPower(fighter) {
    let criticalHitChance = (Math.random() + 1)
    let attack = fighter.power
    console.log("Random = " + criticalHitChance + " DamageDefolt = " + fighter.power)
    let power = attack * criticalHitChance;
    return power
}

export function getBlockPower(fighter) {
    let dodgeChance = (Math.random() + 1)
    console.log("Random = " + dodgeChance + " DefenseDefolt = " + fighter.defense)
    let power = fighter.defense * dodgeChance;
    return power
}

function reloadHPbar(inf, bar) {
    console.log(inf.HP + ' ' + inf.HPbegin)
    if (inf.HP > 0) {
        bar.style.width = parseInt(inf.HP / inf.HPbegin * 100) + '%'
        bar.innerText = parseInt(inf.HP)
    }
    if (inf.HP < 0) {
        bar.innerText = 0
        console.log(bar)
        bar.style.width = "100%"
        bar.style.backgroundColor = "red";
    }
}

function offCombo(info) {
    info.buttComb1 = false;
    info.buttComb2 = false;
    info.buttComb3 = false;
}

function checkCombo(info) {
    return (info.buttComb1 && info.buttComb2 && info.buttComb3)
}

function checkCoolDown(info) {
    if ((Date.now() - info.LastTimeCrit) > 10000) {
        return true
    } else
        return false
}

function animationAttack(WHO) {
    let el = document.getElementsByClassName(`arena___${WHO}-fighter`)[0]
    el.style.position = "relative";
    if (WHO == 'left')
        el.style.left = "450px"
    else
        el.style.right = "450px"
}

function DisableanimationAttack(WHO) {
    let el = document.getElementsByClassName(`arena___${WHO}-fighter`)[0]
    if (WHO == 'left')
        el.style.left = "0px"
    else
        el.style.right = "0px"
}

function animationBlock(WHO) {
    let el = document.getElementsByClassName(`arena___${WHO}-fighter`)[0]
    el.style.position = "relative";
    el.style.top = "65px";
}

function DisableanimationBlock(WHO) {
    let el = document.getElementsByClassName(`arena___${WHO}-fighter`)[0]
    el.style.top = "0px";
}

function animationSuperAttack(WHO) {
    let imgF = document.getElementsByClassName(`fighter-preview___img`)
    console.log(imgF)
    let el = document.getElementsByClassName(`arena___${WHO}-fighter`)[0]
    el.style.position = "relative";
    if (WHO == 'left') {
        imgF[0].style.transform = "scaleX(-1)"
        el.style.left = "1000px"
    } else {
        imgF[1].style.transform = "scaleX(1)"
        el.style.right = "1000px"
    }

}

function DisableanimationSuperAttack(WHO) {
    let imgF = document.getElementsByClassName(`fighter-preview___img`)
    let el = document.getElementsByClassName(`arena___${WHO}-fighter`)[0]
    if (WHO == 'left') {
        imgF[0].style.transform = "scaleX(1)"
        el.style.left = "0px"
    } else {
        imgF[1].style.transform = "scaleX(-1)"
        el.style.right = "0px"
    }

}