const { UserRepository } = require('../repositories/userRepository')

class UserService {

    // TODO: Implement methods to work with user
    create(item){ 
        if(this.search({email: item.email}))
            throw Error("This mail is already taken")
        if(this.search({phoneNumber: item.phoneNumber}))
            throw Error("This phone number is already taken")
        
        return UserRepository.create(item)
    }
    update(ID,data){
        if(data.email&&this.search({email: data.email}))
            throw Error("This mail is already taken")
        if(data.phoneNumber&&this.search({phoneNumber: data.phoneNumber}))
            throw Error("This phone number is already taken")
        if(!this.search({id: ID}))
            throw Error("ID not found")
        else
            return UserRepository.update(ID,data)

    }
    search(search) {
        const item = UserRepository.getOne(search)
        if(!item) {
            return null
        }
        return item
    }
    getAll(){
        const all = UserRepository.getAll()
        return all
    }
    delete(ID){
        const data =  this.search({id: ID})
        if(!data)
            return null
        return UserRepository.delete(ID)[0]
    }
}

module.exports = new UserService()