const { FighterRepository } = require('../repositories/fighterRepository')

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll(){
        return FighterRepository.getAll()
    }
    search(search) {
        const item = FighterRepository.getOne(search)
        if(!item) {
            return null
        }
        return item
    }
    create(item){ 
        if(this.search({name: item.name}))
            throw Error("This name is already taken")
        return FighterRepository.create(item)
    }
    update(ID,data){
        if(this.search({name: data.name}))
            throw Error("This name is already taken")
        if(!this.search({id: ID}))
            throw Error("ID not found")
        else
            return FighterRepository.update(ID,data)
    }
    delete(ID){
        const data =  this.search({id: ID})
        if(!data)
            return null
        return FighterRepository.delete(ID)[0]
    }
}

module.exports = new FighterService()