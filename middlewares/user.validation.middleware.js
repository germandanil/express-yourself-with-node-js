const { user } = require('../models/user')
const createUserValid = (req, res, next) => {   
    try {
    checkSuperfluousField(user,req.body)
    if(!validatEmail(req.body.email))
        throw Error("Email is not gmail.com or entered incorrectly or missing")
    if(!validatNumber(req.body.phoneNumber))
        throw Error("Phone number doesn't start with 380 or entered incorrectly or missing") 
    if(!validatPassword(req.body.password))
        throw Error("Password too short or missing") 
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}
function checkSuperfluousField(user, item){
    if(Object.keys(item).length<Object.keys(user).length-1)
        throw Error("The number of fields is less than required")
    CheckField(user,item)
}
function CheckField(user,item){
    if(item.hasOwnProperty("id"))
        throw Error(`Field ID is must be missing`)
    let num = 0
    for(field in item){
        num++
        
        if(((typeof user[field]) != (typeof item[field]))||!user.hasOwnProperty(field) )
            throw Error(`Field ${field} is superfluous or incorrect type of field`)
    }
    if(num ==0)
        throw Error(`There are no fields`)
}
function validatEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email)
    &&(email.length - email.indexOf("gmail.com")==9) //check gmail.com in the end
}
function validatNumber(num){
    if(parseInt(num/Math.pow(10,10)) == 38)
        return true
    return false
}
function validatPassword(pass){
    if(pass.length<3)
        return false
    return true
}
const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        CheckField(user,req.body)
        if(req.body.email&&!validatEmail(req.body.email))
            throw Error("Email is not gmail.com or entered incorrectly or missing")
        if(req.body.phoneNumber&&!validatNumber(req.body.phoneNumber))
            throw Error("Phone number doesn't start with 380 or entered incorrectly or missing") 
        if(req.body.password&&!validatPassword(req.body.password))
            throw Error("Password too short or missing") 
        } catch (err) {
            res.err = err
        } finally {
            next()
        }
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid

