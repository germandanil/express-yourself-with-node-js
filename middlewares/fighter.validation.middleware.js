const { fighter } = require('../models/fighter')

const createFighterValid = (req, res, next) => {   
    try {
        item = req.body
        checkSuperfluousField(fighter, item)
        if(!item.hasOwnProperty("health"))
            item.health = 100
        if(item.health<80||item.health>120)
            throw Error(`Health must be in range from 80 to 120`)
        if(item.defense< 1 || item.defense > 10)
            throw Error("Defense must be in range from 1 to 10") 
        if(item.power < 1 || item.power > 100)
            throw Error("Power must be in range from 1 to 100") 
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}
function checkSuperfluousField(user, item){
    if(Object.keys(item).length<Object.keys(user).length-2)
        throw Error("The number of fields is less than required")
    CheckField(user,item)
}
function CheckField(user,item){
    if(item.hasOwnProperty("id"))
        throw Error(`Field ID is must be missing`)
    let num = 0
    for(field in item){
        num++
        if(((typeof user[field]) != (typeof item[field]))||!user.hasOwnProperty(field) )
            throw Error(`Field ${field} is superfluous or incorrect type of field`)
        }
    if(num ==0)
        throw Error(`There are no fields`)
}

const updateFighterValid = (req, res, next) => {
    try {
        item = req.body
        CheckField(fighter, item)
        if(item.health&&(item.health<80||item.health>120))
            throw Error(`Health must be in range from 80 to 120`)
        if(item.defense && (item.defense< 1 || item.defense > 10) )
            throw Error("Defense must be in range from 1 to 10") 
        if(item.power&&(item.power < 1 || item.power > 100))
            throw Error("Power must be in range from 1 to 100") 
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}

exports.createFighterValid = createFighterValid
exports.updateFighterValid = updateFighterValid